import random
import re

class Markov(object):
    
    def __init__(self, corpus):
        self.dict = {}
        self.words = corpus.split()
        for idx in range(len(self.words)-1):
            key = self.words[idx]
            word = self.words[idx+1]
            if self.dict.has_key(key):
                self.dict[key].append(word)
            else:
                self.dict[key] = [word]
        
    def get_next(self, inword):
        try:
            inword = inword.strip()
            out = random.choice(self.dict[inword])
            #print "hit"
        except:
            out = random.choice(self.words)
            #print "cache miss"
        return out
