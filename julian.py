#paranoid text generator
import markov
import random
import glob
import mailbox

esc_chars = ["\\", '#','$','%','^','&','_','{',"}","~"]

poe = markov.Markov(open("purloined.txt").read())

train_txt = ""

username = "julian"

#train from email
for archive in glob.glob("w7*"):
    mbox = mailbox.mbox(archive)
    for msg in mbox:
        if username in msg["from"]:
            for part in msg.walk():
                if part.get_content_type() == "text/plain":
                    train_txt += part.get_payload(decode=True)
                    

user = markov.Markov(train_txt)

wc = 0
out = "  "
lastword = " "
while wc < 4000:
    
    #generate paragraph
    for i in range(random.randint(4,12)):
        #generat sentence
        #print "new sentence: "
        while out[-2] != ".":
            if random.choice([True,False]):
                lastword = poe.get_next(lastword)
            else:
                lastword = user.get_next(lastword)
            out += lastword + " "
            #print lastword + " "
            wc += 1

        out += "\n\n"

for ch in esc_chars:
	out = out.replace(ch,"\\"+ch)
print out
